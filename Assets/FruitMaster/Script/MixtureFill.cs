using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixtureFill : MonoBehaviour
{
    // Start is called before the first frame update
    AudioSource au;
     
    private void OnEnable()
    {
        if (au==null)
        {
            au = GetComponent<AudioSource>();
        }
    }

    private void Start()
    {
        if (DataHandler.inst.Sound == 1)
        {
            au.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
