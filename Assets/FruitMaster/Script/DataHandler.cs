using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHandler : MonoBehaviour
{
    public static DataHandler inst;
    public int totalFruits;
    public int cuttedFruits;

    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
    }

    public int Coins
    {
        get
        {
            return PlayerPrefs.GetInt("Coins", 0);
        }
        set
        {
            PlayerPrefs.SetInt("Coins", value);
        }
    }

    public int LevelReached
    {
        get
        {
            return PlayerPrefs.GetInt("LevelReached", 0);
        }
        set
        {
            PlayerPrefs.SetInt("LevelReached", value);
        }
    }

    public int Sound
    {
        get
        {
            return PlayerPrefs.GetInt("Sound", 1);
        }
        set
        {
            PlayerPrefs.SetInt("Sound", value);
        }
    }

    public int Vibrate
    {
        get
        {
            return PlayerPrefs.GetInt("Vibrate", 1);
        }
        set
        {
            PlayerPrefs.SetInt("Vibrate", value);
        }
    }

    public int RatingPanelInt
    {
        get
        {
            return PlayerPrefs.GetInt("RatingPanelInt", 0);
        }
        set
        {
            PlayerPrefs.SetInt("RatingPanelInt", value);
        }

    }

}//class end
