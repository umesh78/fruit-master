﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
public enum state
{ 
    start,playing,pause,win,loose
}

public class GameManager : MonoBehaviour 
{
    public static GameManager inst;

    public state currentState;

    [HideInInspector]
    public int ScoreCount;
    //public Text Score;
    public TextMeshProUGUI score;
    //public Text LevelText;    
    public TextMeshProUGUI levelText;
    public Slider LevelSlider;
    public Transform FruitStorage;
    public GameObject LevComp;
    public GameObject GameOver;
    public GameObject ExitMen;
    float SliderValue;
  
    public GameObject activeFruitCont;
    public List<int> furits = new List<int>();

    public Image whiteImage;

    void Awake () 
    {

        if (inst == null)
        {
            inst = this;
        }

        //if (PlayerPrefs.GetInt("first") == 0)
        //{
        //    PlayerPrefs.SetInt("LevCount", 1);
        //    PlayerPrefs.SetInt("first", 1);
        //}
        levelText.text = "LEVEL " + (DataHandler.inst.LevelReached + 1);
        Debug.Log("Leeeeeeeeeveeeeel" + DataHandler.inst.LevelReached);
    }

    private void Start()
    {
        currentState = state.start;
        StartCoroutine(CheckInternetConnection(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                whiteImage.transform.gameObject.SetActive(true);
            }
            else
            {
                Debug.Log("Internet Not Available");
                whiteImage.transform.gameObject.SetActive(false);
            }
        }));
    }

    void Update () 
    {
        LevelSlider.value = Mathf.Lerp(LevelSlider.value, SliderValue, 10f * Time.smoothDeltaTime);
        //if (Input.GetKeyDown(KeyCode.Escape) && FindObjectOfType<KnifeScript>().Playable ==1)
        //{
        //    ExitMen.SetActive(true);
        //}
    }
    public void ScoreSet()
    {       
        score.text = ScoreCount.ToString();
    }

    public void SliderSet()
    {
        if (LevelSlider == null)
        {
            Debug.LogError("Null Null NUll");
        }
        if (LevelSlider.value < 0.98f)
        {
            //SliderValue = SliderValue + .1f / (PlayerPrefs.GetInt("LevCount"));

            SliderValue = SliderValue +  MixerScript.inst.x / MixerScript.inst.TotalFruit;
            //Debug.Log(SliderValue + "HetavS1");
            if (SliderValue>=0.99f)
            {
                Debug.LogError("umesh");
                currentState = state.win;
                UiManager.inst.winScreen.SetActive(true);
                if (DataHandler.inst.Sound == 1)
                {
                    UiManager.inst.winScreen.transform.GetChild(0).GetComponent<AudioSource>().Play();
                }
                Invoke("InvokeNextButton", 2f);
                //PlayerPrefs.SetInt("LevCount", PlayerPrefs.GetInt("LevCount") + 1);
                DataHandler.inst.LevelReached += 1;
            }
        }
        else
        {
            LevComp.SetActive(true);
            //PlayerPrefs.SetInt("LevCount", PlayerPrefs.GetInt("LevCount") + 1);
            DataHandler.inst.LevelReached += 1;
        }       
    }

    public void InvokeNextButton()
    {
        UiManager.inst.winNextLevelButton.SetActive(true);    
    }

    public void SliderLoad()
    {
        SliderValue = 0;
        levelText.text = "LEVEL " + (DataHandler.inst.LevelReached + 1);
    }
    public void Reload()
    {
        
    }

    IEnumerator CheckInternetConnection(Action<bool> action)
    {
        UnityWebRequest request = new UnityWebRequest("http://google.com");
        yield return request.SendWebRequest();
        if (request.error != null)
        {
            Debug.Log("Error");
            action(false);
        }
        else
        {
            Debug.Log("Success");
            action(true);
        }
    }

}//class end
