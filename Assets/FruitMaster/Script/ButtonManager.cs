﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonManager : MonoBehaviour {

    public GameObject KnifeSel;
    bool knifeMenAct;
    public int needToUnlockKnife;
    public Image knifeImg;
    public TextMeshProUGUI unlockText;

	void Start () 
    {
        //if (PlayerPrefs.GetInt("KnifeSel") == 0)
        //{
        //    unlockText.transform.gameObject.SetActive(false);
        //    knifeImg.rectTransform.localPosition = new Vector3(0, 0, 0);
        //}
        if (unlockText != null)
        {
            if (PlayerPrefs.GetInt("KnifeSel" + gameObject.name) == 1)
            {
                unlockText.transform.gameObject.SetActive(false);
                knifeImg.rectTransform.localPosition = new Vector3(0, 0, 0);
            }
        }
    }
	
	void Update () {
        //if (Input.GetKeyDown(KeyCode.Escape) && knifeMenAct == true)
        //{
        //    knifeMenAct = false;
        //    GameObject.Find("BackBTN").transform.parent.GetComponent<Animator>().SetTrigger("KnifeMenuBack");
        //    if (DataHandler.inst.Sound == 1)
        //    {
        //        GameObject.Find("BackBTN").GetComponent<AudioSource>().Play();
        //    }
        //}
    }

    public void NoThanks()
    {
        if (DataHandler.inst.Sound == 1)
        {
            transform.parent.parent.GetComponent<AudioSource>().Play();
        }

        transform.parent.gameObject.SetActive(false);
        Camera.main.GetComponent<Animator>().SetInteger("Playable", 0);

        if (DataHandler.inst.Sound == 1)
            Camera.main.GetComponent<AudioSource>().Play();

        if (GameObject.Find("Drink"))
        {
            GameObject.Find("Drink").SetActive(false);
        }
        FindObjectOfType<GameManager>().SliderLoad();
        FindObjectOfType<GameManager>().ScoreCount = 0;
        FindObjectOfType<GameManager>().ScoreSet();
        FindObjectOfType<MixerScript>().DesFruitCon();
        FindObjectOfType<KnifeScript>().Playable = 0;
        FindObjectOfType<KnifeScript>().GetComponent<SpriteRenderer>().enabled = true;
        //FindObjectOfType<AdMobManager>().showInterstitial();
    }
    public void WatchVideo()
    {
        //if (DataHandler.inst.Sound == 1)
        //    transform.parent.parent.GetComponent<AudioSource>().Play();

        //FindObjectOfType<AdMobManager>().Revive = true;
        //FindObjectOfType<AdMobManager>().BTN = GetComponent<ButtonManager>();
        ////FindObjectOfType<AdMobManager>().showInterstitial();

        //FindObjectOfType<AdMobManager>().waitingPanel.SetActive(true);
        //Invoke("InvokeShowAd", Random.Range(1.5f, 2.5f));

    }

    public void InvokeShowAd()
    {
        //FindObjectOfType<AdMobManager>().ShowAd();
        //FindObjectOfType<AdMobManager>().waitingPanel.SetActive(false);
    }

    public void RewardVideo()
    {
        FindObjectOfType<KnifeScript>().GetComponent<SpriteRenderer>().enabled = true;
        transform.parent.gameObject.SetActive(false);
    }
    public void NextLevel ()
    {
        if (GameObject.Find("Drink"))
        {
            GameObject.Find("Drink").SetActive(false);
        }
        if (DataHandler.inst.Sound == 1)
        {
            transform.parent.parent.GetComponent<AudioSource>().Play();
            Camera.main.GetComponent<AudioSource>().Play();
        }
        transform.parent.gameObject.SetActive(false);
        FindObjectOfType<MixerScript>().DesFruitCon();
        FindObjectOfType<KnifeScript>().Playable = 0;
        PlayerPrefs.SetInt("LevCount", PlayerPrefs.GetInt("LevCount") + 1);
        Camera.main.GetComponent<Animator>().SetInteger("Playable", 0);
        FindObjectOfType<GameManager>().SliderLoad();
        //FindObjectOfType<AdMobManager>().showInterstitial();
    }

    public void KnifeSelect()
    {
        if (PlayerPrefs.GetInt("KnifeSel" + gameObject.name) == 1)
        {
            PlayerPrefs.SetInt("KnifeSel", int.Parse(gameObject.name));
            //if (PlayerPrefs.GetInt("KnifeSel") == 8)
            //{
            //    transform.parent.parent.parent.GetChild(2).position = new Vector3(0,640,0);
            //}
            //else
            //{
            //    transform.parent.parent.parent.GetChild(2).position = new Vector3(0, 570, 0);
            //}
            transform.parent.parent.parent.GetChild(2).GetComponent<Image>().sprite = transform.GetChild(0).GetComponent<Image>().sprite;
            if (DataHandler.inst.Sound == 1)
            {
                GetComponent<AudioSource>().Play();
            }
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                transform.parent.GetChild(i).GetComponent<KnifeSel>().Check();
            }
            FindObjectOfType<KnifeScript>().KnifeSpriteChange();
            //unlockText.transform.gameObject.SetActive(false);
            //knifeImg.rectTransform.localPosition = new Vector3(0, 0, 0);
        }
        else
        {
            if (PlayerPrefs.GetInt("Coins") >= needToUnlockKnife)
            {
                if (DataHandler.inst.Sound == 1)
                {
                    transform.parent.GetComponent<AudioSource>().Play();
                }
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - needToUnlockKnife);
                FindObjectOfType<CoinScript>().cointest();
                //transform.parent.parent.GetChild(3).gameObject.SetActive(true);
                //transform.parent.parent.GetChild(3).position = Input.mousePosition;
                PlayerPrefs.SetInt("KnifeSel" + gameObject.name, 1);
                PlayerPrefs.SetInt("KnifeSel", int.Parse(gameObject.name));
                transform.parent.parent.parent.GetChild(2).GetComponent<Image>().sprite = transform.GetChild(0).GetComponent<Image>().sprite;
                FindObjectOfType<KnifeScript>().KnifeSpriteChange();
                unlockText.transform.gameObject.SetActive(false);
                knifeImg.rectTransform.localPosition = new Vector3(0, 0, 0);
                for (int i = 0; i < transform.parent.childCount; i++)
                {
                    transform.parent.GetChild(i).GetComponent<KnifeSel>().Check();
                }
                //if (PlayerPrefs.GetInt("KnifeSel") == 8)
                //{
                //    transform.parent.parent.parent.GetChild(2).position = new Vector3(0, 640, 0);
                //}
                //else
                //{
                //    transform.parent.parent.parent.GetChild(2).position = new Vector3(0, 570, 0);
                //}
            }
            else
            {
                if (DataHandler.inst.Sound == 1)
                {
                    transform.GetChild(0).GetComponent<AudioSource>().Play();
                }
                transform.parent.parent.GetChild(0).transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "YOU NEED MORE ";
            }
        }
    }

    public void KnifeCustomize()
    {
        if (DataHandler.inst.Sound == 1)
        {
            GetComponent<AudioSource>().Play();
        }
        knifeMenAct = true;
        KnifeSel.SetActive(true);
    }

    public void KnifeCustomizeBack()
    {
        if (DataHandler.inst.Sound == 1)
        {
            GetComponent<AudioSource>().Play();
        }
        knifeMenAct = false;
        transform.parent.GetComponent<Animator>().SetTrigger("KnifeMenuBack");
    }

    public void ExitNo()
    {
        if (DataHandler.inst.Sound == 1)
        {
            transform.parent.parent.parent.GetComponent<AudioSource>().Play();
        }
        transform.parent.parent.gameObject.SetActive(false);
    }

    public void ExitYes()
    {
        if (DataHandler.inst.Sound == 1)
            transform.parent.parent.parent.GetComponent<AudioSource>().Play();
        transform.parent.parent.gameObject.SetActive(false);
        FindObjectOfType<MixerScript>().DesFruitCon();
        FindObjectOfType<KnifeScript>().Playable = 0;
        FindObjectOfType<GameManager>().SliderLoad();
        Camera.main.GetComponent<Animator>().SetInteger("Playable", 0);
        FindObjectOfType<GameManager>().ScoreCount = 0;
        FindObjectOfType<GameManager>().ScoreSet();
    }

}//class end
