﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CoinScript : MonoBehaviour {

    public static CoinScript inst;
    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
    }
    public GameObject CoinSound;
    public TextMeshProUGUI coinCountText;

	void Start ()
    {
        cointest();
    }
	
	void Update ()
    {
       
    }

    public void cointest()
    {
        //transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt("Coins").ToString();
        coinCountText.text = DataHandler.inst.Coins.ToString();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
            Debug.Log(DataHandler.inst.Coins);
        if (col.gameObject.tag == "Coin" )
        {
            GameObject ab = Instantiate(CoinSound, transform.position, Quaternion.identity);
            Destroy(ab, 1);
            //ab.transform.position = Vector3.MoveTowards(transform.position, coinObj.transform.position,speed * Time.deltaTime);
            //GetComponent<Animator>().SetTrigger("CoinCollected");
            Destroy(col.gameObject);
            //PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") + 1);
            DataHandler.inst.Coins += 1;
            //transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt("Coins").ToString();
            coinCountText.text = DataHandler.inst.Coins.ToString();
        }
    }

}//class end
