﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomDrinkSprite : MonoBehaviour
{
    [SerializeField]
    Sprite[] DrinkGlass;
    public int coinCount = 3;
    public GameObject Coin;
    int coincon;
    public Transform startPos;

    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = DrinkGlass[Random.Range(0, DrinkGlass.Length)];
    }

    public void Deactive()
    {
        GetComponent<SpriteRenderer>().sprite = DrinkGlass[Random.Range(0, DrinkGlass.Length)];
        gameObject.SetActive(false);
    }

    public void Coins()
    {
        if (DataHandler.inst.Sound == 1)
        {
            transform.GetChild(0).GetComponent<AudioSource>().Play();
        }
        VFX_Coins.Instance.startPoint = this.transform;
        VFX_Coins.Instance.AddCoins(3);
    }
}
