using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class VFXCoinsMove : MonoBehaviour
{
    public void StartMoving(Vector3 fpos, Vector3 endPos)
    {
        transform.DOKill();
        transform.DOLocalMove(fpos , 0.2f).SetEase(Ease.OutSine).OnComplete(() =>
        {
            transform.DOMove(endPos, 0.35f).SetEase(Ease.OutSine).OnComplete(() =>
            {
                DataHandler.inst.Coins += 1;
                CoinScript.inst.cointest();
                gameObject.SetActive(false); 
            });
        });
    }
}
