﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZCameraShake;


public class KnifeScript : MonoBehaviour {
    [HideInInspector]
    public int FruitsDes;
    bool clicked;
    //[SerializeField]
    //GameObject Splash;
    [SerializeField]
    Sprite Circle;
    //public GameObject Vignette;
    public Sprite[] Knifes;

    public int Playable;
    //public bool cantPlay;
    public int firstKnife;

    void Start() {
        FruitsDes = 0;
        //Vignette.SetActive(false);
        KnifeSpriteChange();
        firstKnife = 1;
        this.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
    }

    public void KnifeSpriteChange()
    {
        GetComponent<SpriteRenderer>().sprite = Knifes[PlayerPrefs.GetInt("KnifeSel")];
    }

    void Update() {
        if (Input.GetMouseButtonDown(0) && GameManager.inst.currentState == state.playing)
        {
            if (firstKnife == 1)
            {
                Playable = 1;
            }
            else
            {
                Playable = 0;
            }
            GameObject thisButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
            if (thisButton == null)
            {
                if (Playable == 0)
                {
                    Playable++;
                    Camera.main.GetComponent<Animator>().SetInteger("Playable", 1);
                    //FindObjectOfType<MixerScript>().InsFruitCon();
                }
                else
                {
                    if (clicked == false)
                    {
                        clicked = true;
                        if (DataHandler.inst.Sound == 1)
                        {
                            GetComponent<AudioSource>().Play();
                        }
                        GetComponent<TrignometricMovement>().enabled = false;
                        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                        GetComponent<Rigidbody2D>().AddForce(Vector3.up * 1300, ForceMode2D.Force);
                        Destroy(gameObject, 1f);
                        Invoke("Inst", .3f);
                    }
                }
            }
        }
        if (clicked)
        {
            transform.Rotate(0, 0, -20);
        }
    }
    void Inst()
    {
        //if (GameManager.inst.currentState == state.playing)
        //{
            //Playable = 1;
            GameObject ab = Instantiate(this.gameObject, new Vector3(0, -2.5f, 0), Quaternion.identity);
            ab.transform.parent = UiManager.inst.levelContainer.transform;
            ab.GetComponent<TrignometricMovement>().enabled = true;
            firstKnife++;
            ab.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        //}
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Fruits")
        {
            DataHandler.inst.cuttedFruits += 1;
            if (DataHandler.inst.totalFruits == DataHandler.inst.cuttedFruits)
            {
                GameManager.inst.currentState = state.pause;
            }
            if (DataHandler.inst.Sound == 1)
            {
                col.gameObject.GetComponent<AudioSource>().Play();
            }
            col.gameObject.GetComponent<Collider2D>().enabled = false;
            col.gameObject.GetComponent<SpriteRenderer>().sprite = Circle;
            col.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, .5f);
        }
        if (col.gameObject.tag == "Missed")
        {
            if (FruitsDes == 0)
            {
                //cantPlay = true;
                GameManager.inst.currentState = state.loose;
                UiManager.inst.knifeMissedScreen.SetActive(true);
                if (DataHandler.inst.Sound ==1)
                {
                    UiManager.inst.knifeMissedScreen.transform.GetChild(0).GetComponent<AudioSource>().Play();
                }
                Invoke("gameOv", .5f);
                FindObjectOfType<KnifeScript>().GetComponent<SpriteRenderer>().enabled = false;
                CameraShaker.Instance.ShakeOnce(2f, 30, 0, .5f);
                if (DataHandler.inst.Vibrate == 1)
                {
                    Handheld.Vibrate();
                }
                //Vignette.SetActive(false);
                //Vignette.SetActive(true);
            }
        }
    }

    void gameOv()
    {
        //FindObjectOfType<GameManager>().GameOver.SetActive(true);
        //cantPlay = false;
        GameManager.inst.currentState = state.loose;
        UiManager.inst.looseScreen.SetActive(true);
        UiManager.inst.knifeMissedScreen.SetActive(false);
    }

}//class end
