﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixerScript : MonoBehaviour {

    public static MixerScript inst;

    public int FruitCount;
    public int TotalFruit;
    GameManager gm;
    public float x = 0.0f;

    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
    }

    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Cutted")
        {
            transform.GetChild(0).GetChild(FruitCount).gameObject.SetActive(true);
            transform.GetChild(0).GetChild(FruitCount).GetComponent<SpriteRenderer>().color = col.transform.GetChild(0).GetComponent<ParticleSystem>().main.startColor.color;
            col.transform.GetChild(0).gameObject.SetActive(true);
            col.transform.GetChild(0).position = new Vector3(-1.8f, -1.8f, 0);
            col.transform.GetChild(0).eulerAngles = Vector3.zero;
            GetComponent<Animator>().SetTrigger("FruitHit");
            Destroy(col.transform.GetChild(0).gameObject, 1);
            col.transform.GetChild(0).parent = null;
            Destroy(col.gameObject, .02f);
            FruitCount++;
            if (TotalFruit == FruitCount)
            {
                //FindObjectOfType<KnifeScript>().cantPlay = true;
                //GameManager.inst.currentState = state.loose;
                if (DataHandler.inst.Sound == 1)
                {
                    GetComponent<AudioSource>().Play();
                }
                FruitCount = 0;
                if (GameManager.inst.currentState != state.win)
                {
                    GameManager.inst.currentState = state.pause;
                    Invoke("FruitDesIns", .8f);
                }
                else
                {
                    transform.parent.GetChild(0).gameObject.SetActive(true);
                    for (int i = 0; i < transform.GetChild(0).childCount; i++)
                    {
                        transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
                    }
                }
            }

        }
        if (col.gameObject.tag == "CuttedExtra")
        {
            col.transform.GetChild(0).gameObject.SetActive(true);
            col.transform.GetChild(0).position = new Vector3(-1.8f, -1.6f, 0);
            col.transform.GetChild(0).eulerAngles = Vector3.zero;
            Destroy(col.transform.GetChild(0).gameObject, 1);
            col.transform.GetChild(0).parent = null;
            Destroy(col.gameObject, .02f);
        }
    }

    void FruitDesIns()
    {
        //FindObjectOfType<KnifeScript>().cantPlay = false;
        //GameManager.inst.currentState = state.playing;
        Debug.Log("CreaatFood");
        GameManager.inst.currentState = state.playing;
        //if (Camera.main.GetComponent<Animator>().GetInteger("Playable") == 1)
        //{
        Destroy(gm.activeFruitCont);
        x = (float)1 / (float)gm.furits[DataHandler.inst.LevelReached];
        //Debug.Log(x + "Hetav1");
        gm.activeFruitCont = Instantiate(gm.FruitStorage.GetChild(Random.Range(gm.FruitStorage.childCount - 1, 0)).gameObject, new Vector3(0f, 1.8f, 0f), Quaternion.identity);
        gm.activeFruitCont.SetActive(true);
        gm.activeFruitCont.transform.parent = UiManager.inst.fruitContainer.transform;
        TotalFruit = gm.activeFruitCont.transform.childCount;
        DataHandler.inst.totalFruits = TotalFruit;
        DataHandler.inst.cuttedFruits = 0;
        transform.parent.GetChild(0).gameObject.SetActive(true);
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        //}       
    }

    public void InsFruitCon ()
    {
        if (GameManager.inst.currentState == state.playing)
        {
            Destroy(gm.activeFruitCont);
            //Debug.Log(PlayerPrefs.GetInt("LevCount") + "Level");
            Debug.Log("Level" + DataHandler.inst.LevelReached);
            x = (float)1 / (float)gm.furits[DataHandler.inst.LevelReached];
            //Debug.Log(x + "Hetav2");
            gm.activeFruitCont = Instantiate(gm.FruitStorage.GetChild(Random.Range(gm.FruitStorage.childCount - 1, 0)).gameObject, new Vector3(0f, 1.8f, 0f), Quaternion.identity);
            gm.activeFruitCont.SetActive(true);
            gm.activeFruitCont.transform.parent = UiManager.inst.fruitContainer.transform;
            Debug.Log(gm.activeFruitCont);
            TotalFruit = gm.activeFruitCont.transform.childCount;
            DataHandler.inst.totalFruits = TotalFruit;
            DataHandler.inst.cuttedFruits = 0;
        }
    }

    public void DesFruitCon()
    {
        FruitCount = 0;
        Destroy(gm.activeFruitCont);
        TotalFruit = 0;
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
    }

}//class end
