using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener
{
    //fruitSlicerKing
    public static AdManager inst;
    public string bannerId_Android;
    public string interstitialId_Android;
    public string videoId_Android;

    void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        Start();
    }

    void Start()
    {
        Advertisement.Initialize("4558827", false, true, this);
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);

        Invoke("LoadBanner",1f);
        Invoke("LoadINterstitial",1.5f);
        Invoke("LoadVideo", 2f); 
    }

    public void OnInitializationComplete()
    {
        
    }

    public bool isInterstitislReady()
    {
        return Advertisement.IsReady(interstitialId_Android);
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }

    public void LoadINterstitial()
    {
        Advertisement.Load(interstitialId_Android, this);
    }

    public void OnUnityAdsAdLoaded(string adUnitId)
    {
        // Optionally execute code if the Ad Unit successfully loads content.
    }

    public void OnUnityAdsFailedToLoad(string adUnitId, UnityAdsLoadError error, string message)
    {
        if (adUnitId == interstitialId_Android)
        {
            Invoke("LoadINterstitial", 3);
        }
        Debug.Log($"Error loading Ad Unit: {adUnitId} - {error.ToString()} - {message}");
        // Optionally execite code if the Ad Unit fails to load, such as attempting to try again.
    }

    public void OnUnityAdsShowFailure(string adUnitId, UnityAdsShowError error, string message)
    {
        Debug.Log($"Error showing Ad Unit {adUnitId}: {error.ToString()} - {message}");
        // Optionally execite code if the Ad Unit fails to show, such as loading another ad.
    }

    public void OnUnityAdsShowStart(string adUnitId)
    {
        Debug.LogError("Unity ad show start ... AdUnitId : " + adUnitId);
    }
    public void OnUnityAdsShowClick(string adUnitId) { }
    public void OnUnityAdsShowComplete(string adUnitId, UnityAdsShowCompletionState showCompletionState)
    {
        Debug.LogError("Unity ad show complete ... AdUnitId : " + adUnitId);
        if (adUnitId == videoId_Android)
        {
            //AdManager.Instance.GiveRewardToUser();
            switch (videoForMain)
            {
                case "Revive":
                    Debug.Log("ReviveHere");
                    UiManager.inst.looseScreen.SetActive(false);
                    if (GameObject.Find("Drink"))
                    {
                        GameObject.Find("Drink").SetActive(false);
                    }
                    FindObjectOfType<KnifeScript>().Playable = 0;
                    FindObjectOfType<KnifeScript>().GetComponent<SpriteRenderer>().enabled = true;
                    UiManager.inst.gamePlayScreen.SetActive(true);
                    GameManager.inst.currentState = state.playing;
                    break;
                case "FreeCoins":
                    Debug.Log("FreeCoinsHere");
                    GameManager.inst.currentState = state.start;
                    if (GameObject.Find("Drink"))
                    {
                        GameObject.Find("Drink").SetActive(false);
                    }
                    MixerScript.inst.FruitCount = 0;
                    FindObjectOfType<KnifeScript>().Playable = 0;
                    Camera.main.GetComponent<Animator>().SetInteger("Playable", 0);
                    int i = Random.Range(10, 20);
                    DataHandler.inst.Coins += i;
                    CoinScript.inst.coinCountText.text = DataHandler.inst.Coins.ToString();
                    GameManager.inst.SliderLoad();
                    UiManager.inst.winScreen.SetActive(false);
                    UiManager.inst.gamePlayScreen.SetActive(false);
                    UiManager.inst.startScreen.SetActive(true);
                    break;
                default:
                    break;
            }
            Invoke("LoadVideo", 1f);
        }
    }

    public void LoadVideo()
    {
        Advertisement.Load(videoId_Android, this);
    }

    public void ShowInterstitial()
    {
        //if (DataHandler.inst.NoAds != 1)
        //{
            if (Advertisement.IsReady(interstitialId_Android))
            {
                Advertisement.Show(interstitialId_Android);
            }
            else
            {
                Debug.LogError("interstitial ad is not ready");
                LoadINterstitial();
            }
        //}
    }

    string videoForMain = "";
    public void ShowRewardVideo(string videoFor)
    {
        videoForMain = videoFor;
        if (Advertisement.IsReady(videoId_Android))
        {
            Advertisement.Show(videoId_Android, this);
        }
        else
        {
            Debug.LogError("reward video ad is not ready");
            LoadVideo();
        }
    }

    public void CheckLoadInterstitial()
    {
        if (!Advertisement.IsReady(interstitialId_Android))
        {
            Debug.LogError("interstitial ad is not ready");
            LoadINterstitial();
        }
    }
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        Debug.LogError("Unity ad show complete ... AdUnitId : " + placementId + "  " + showResult);
        // Define conditional logic for each ad completion status:
        if (placementId == videoId_Android)
        {
            if (showResult == ShowResult.Finished)
            {
                switch (videoForMain)
                {
                    case "Revive":
                        Debug.Log("ReviveHere");
                        UiManager.inst.looseScreen.SetActive(false);
                        if (GameObject.Find("Drink"))
                        {
                            GameObject.Find("Drink").SetActive(false);
                        }
                        FindObjectOfType<KnifeScript>().Playable = 0;
                        FindObjectOfType<KnifeScript>().GetComponent<SpriteRenderer>().enabled = true;
                        UiManager.inst.gamePlayScreen.SetActive(true);
                        GameManager.inst.currentState = state.playing;
                        break;
                    default:
                        break;
                } 
            }
            else if (showResult == ShowResult.Skipped)
            {
                // Do not reward the user for skipping the ad.
            }
            else if (showResult == ShowResult.Failed)
            {
            }
            // Reward the user for watching the ad to completion.
            Invoke("LoadVideo", 2);

        }
        else if (placementId == interstitialId_Android)
        {
            Invoke("LoadINterstitial", 2f);
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, show the ad:
        //if (placementId == myPlacementId)
        //{
        //    Advertisement.Show(myPlacementId);
        //}
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    public bool IsRewardLoaded()
    {
        return Advertisement.IsReady(videoId_Android);
    }

    public void LoadBanner()
    {
        //if (DataHandler.inst.NoAds != 1)
        //{
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);

            // Set up options to notify the SDK of load events:
            BannerLoadOptions options = new BannerLoadOptions
            {
                loadCallback = OnBannerLoaded,
                errorCallback = OnBannerError
            };

            // Load the Ad Unit with banner content:
            Advertisement.Banner.Load(bannerId_Android, options);
        //}
    }

    void OnBannerLoaded()
    {
        Debug.Log("Banner loaded");
        //ShowBannerAd();
    }

    // Implement code to execute when the load errorCallback event triggers:
    void OnBannerError(string message)
    {
        Debug.Log($"Banner Error: {message}");
        Invoke("LoadBanner", 5);
        // Optionally execute additional code, such as attempting to load another ad.
    }

    // Implement a method to call when the Show Banner button is clicked:
    public void ShowBannerAd()
    {
        // Set up options to notify the SDK of show events:
        BannerOptions options = new BannerOptions
        {
            hideCallback = OnBannerHidden,
            showCallback = OnBannerShown
        };

        // Show the loaded Banner Ad Unit:
        Advertisement.Banner.Show(bannerId_Android, options);
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
    }

    void OnBannerShown() { }
    void OnBannerHidden() { }
    public void HideBannerAd()
    {
        // Hide the banner:
        Advertisement.Banner.Hide();
    }
}
