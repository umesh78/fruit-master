using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static UiManager inst;

    [Header("Screens")]
    public GameObject splashScreenPanel;
    public GameObject startScreen;
    public GameObject gamePlayScreen;
    public GameObject knifeShopScreen;
    public GameObject winScreen;
    public GameObject looseScreen;
    public GameObject knifeMissedScreen;
    public GameObject waitingScreen;
    public GameObject ratingScreen;
    public GameObject exitGamePlayScreen;
    public GameObject exitStartScreen;

    //public Transform bazierEndPoint;
    public Transform coinTransform;
    public GameObject winNextLevelButton;
    public GameObject levelContainer;
    public GameObject fruitContainer;
    public List<GameObject> stars = new List<GameObject>();

    public GameObject ratingCancleButton;
    public GameObject ratingSubmitButton;

    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
    }

    private void Start()
    {
        if (DataHandler.inst.Sound == 1)
        {
            soundOffObj.SetActive(false);
            soundOffObjPause.SetActive(false);
        }
        else
        {
            soundOffObj.SetActive(true);
            soundOffObjPause.SetActive(true);
        }

        if (DataHandler.inst.Vibrate == 1)
        {
            vibrateOffObj.SetActive(false);
            vibrateOffObjPause.SetActive(false);
        }
        else
        {
            vibrateOffObj.SetActive(true);
            vibrateOffObjPause.SetActive(true);
        }
    }

    private void Update()
    {
        if (gamePlayScreen.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && GameManager.inst.currentState == state.playing)
            {
                GameManager.inst.currentState = state.pause;
                exitGamePlayScreen.SetActive(true);
            }
        }

        if (startScreen.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                exitStartScreen.SetActive(true);
            }
        }

        //if (SplashScreen.inst.fillImage.fillAmount == 1)
        //{
        //    Debug.LogError(111);
        //    splashScreenPanel.SetActive(false);
        //    AdManager.inst.ShowBannerAd();
        //}
    }

    public void StartButton()
    {
        GameManager.inst.currentState = state.playing;
        startScreen.SetActive(false);
        gamePlayScreen.SetActive(true);
        MixerScript.inst.InsFruitCon();
    }

    public void KnifeShopButton()
    {
        startScreen.SetActive(false);
        knifeShopScreen.SetActive(true);
        fruitContainer.SetActive(false);
    }

    public void KnifeShopBackButton()
    {
        knifeShopScreen.SetActive(false);
        startScreen.SetActive(true);
        fruitContainer.SetActive(true);
    }

    public void WinNextLevelButton()
    {
        //if (GameObject.Find("Drink"))
        //{
        //    GameObject.Find("Drink").SetActive(false);
        //}
        //transform.parent.parent.GetComponent<AudioSource>().Play();
        //Camera.main.GetComponent<AudioSource>().Play();
        //transform.parent.gameObject.SetActive(false);
        //MixerScript.inst.DesFruitCon();
        //PlayerPrefs.SetInt("LevCount", PlayerPrefs.GetInt("LevCount") + 1);
        winNextLevelButton.SetActive(false);
        GameManager.inst.currentState = state.start;
        MixerScript.inst.FruitCount = 0;
        FindObjectOfType<KnifeScript>().Playable = 0;
        Camera.main.GetComponent<Animator>().SetInteger("Playable", 0);
        GameManager.inst.SliderLoad();
        winScreen.SetActive(false);
        //MixerScript.inst.InsFruitCon();
        gamePlayScreen.SetActive(false);
        int i = Random.Range(0, 2);
        if (i == 1 && DataHandler.inst.LevelReached % 3 != 0) 
        {
            if (AdManager.inst.isInterstitislReady())
            {
                waitingScreen.SetActive(true);
                GameManager.inst.currentState = state.start;
            }
            Invoke("ShowInterAd", 1.5f);
        }
        startScreen.SetActive(true);
        if (DataHandler.inst.LevelReached % 3 == 0)
        {
            if (DataHandler.inst.RatingPanelInt == 0)
            {
                ratingScreen.SetActive(true);
            }
        }
    }

    public void WinVideoCoinButton()
    {
        Debug.Log("VideoHere");
        AdManager.inst.ShowRewardVideo("FreeCoins");
    }

    public void LooseContinueButton()
    {
        Debug.Log("VideoHere");
        AdManager.inst.ShowRewardVideo("Revive");
    }

    public void LooseNoThanksButton()
    {
        Camera.main.GetComponent<Animator>().SetInteger("Playable", 0);
        if (DataHandler.inst.Sound == 1)
        {
            Camera.main.GetComponent<AudioSource>().Play();
        }
        if (GameObject.Find("Drink"))
        {
            GameObject.Find("Drink").SetActive(false);
        }
        GameManager.inst.SliderLoad();
        GameManager.inst.ScoreCount = 0;
        GameManager.inst.ScoreSet();
        MixerScript.inst.DesFruitCon();
        FindObjectOfType<KnifeScript>().Playable = 0;
        FindObjectOfType<KnifeScript>().GetComponent<SpriteRenderer>().enabled = true;
        GameManager.inst.currentState = state.start;
        looseScreen.SetActive(false);
        gamePlayScreen.SetActive(false);
        int i = Random.Range(0, 2);
        if (i == 1 && DataHandler.inst.LevelReached % 3 != 0)
        {
            if (AdManager.inst.isInterstitislReady())
            {
                waitingScreen.SetActive(true);
                GameManager.inst.currentState = state.start;
            }
            Invoke("ShowInterAd", 1.5f);
        }
        startScreen.SetActive(true);
    }

    public void ShowInterAd()
    {
        AdManager.inst.ShowInterstitial();
        waitingScreen.SetActive(false);
    }

    [Header("Setting referances")]

    public GameObject settingPanel;
    public GameObject soundOffObj;
    public GameObject soundOffObjPause;

    public GameObject vibrateOffObj;
    public GameObject vibrateOffObjPause;
    public Animator animSetting;
    public void SettingClick()
    {
        animSetting.SetTrigger("Enable");
      
    }

    public void SettingSoundClick()
    {
        if (DataHandler.inst.Sound == 1)
        {
            DataHandler.inst.Sound = 0;
            soundOffObj.SetActive(true);
            soundOffObjPause.SetActive(true);
        }
        else
        {
            DataHandler.inst.Sound = 1;
            soundOffObj.SetActive(false);
            soundOffObjPause.SetActive(false);
        }
    }

    public void SettingVibrateClick()
    {
        if (DataHandler.inst.Vibrate == 1)
        {
            DataHandler.inst.Vibrate = 0;
            vibrateOffObj.SetActive(true);
            vibrateOffObjPause.SetActive(true);
        }
        else
        {
            DataHandler.inst.Vibrate = 1;
            vibrateOffObj.SetActive(false);
            vibrateOffObjPause.SetActive(false);
        }
    }

    public void SettingClose()
    {
        animSetting.SetTrigger("Disable");
         
    }

    public void ExitYesButton()
    {
        //if (DataHandler.inst.Sound == 1)
        //    transform.parent.parent.parent.GetComponent<AudioSource>().Play();
        GameManager.inst.currentState = state.start;
        gamePlayScreen.SetActive(false);
        exitGamePlayScreen.SetActive(false);
        startScreen.SetActive(true);
        MixerScript.inst.DesFruitCon();
        FindObjectOfType<KnifeScript>().Playable = 0;
        GameManager.inst.SliderLoad();
        Camera.main.GetComponent<Animator>().SetInteger("Playable", 0);
        GameManager.inst.ScoreCount = 0;
        GameManager.inst.ScoreSet();
    }

    public void ExitNoButton()
    {
        GameManager.inst.currentState = state.playing;
        gamePlayScreen.SetActive(true);
        exitGamePlayScreen.SetActive(false);
    }

    public void ExitBackButton()
    {
        GameManager.inst.currentState = state.playing;
        gamePlayScreen.SetActive(true);
        exitGamePlayScreen.SetActive(false);
    }

    public void RatingStars(int j)
    {
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].transform.GetComponent<Image>().color = Color.black;
            if (i < j)
            {
                stars[i].transform.GetComponent<Image>().color = Color.white;
                ratingCancleButton.SetActive(false);
                ratingSubmitButton.SetActive(true);
            }
        }
    }

    public void RatingCancleButtonClick()
    {
        ratingScreen.SetActive(false);
        startScreen.SetActive(true);
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].transform.GetComponent<Image>().color = Color.black;
        }
        ratingSubmitButton.SetActive(false);
        ratingCancleButton.SetActive(true);
    }

    public void RatingSubmitButtonClick()
    {
        Debug.Log("PlayStoreOpenHere");
        DataHandler.inst.RatingPanelInt = 1;
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.leopardgames.fruitslicerking");
        ratingScreen.SetActive(false);
        startScreen.SetActive(true);
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].transform.GetComponent<Image>().color = Color.black;
        }
        ratingSubmitButton.SetActive(false);
        ratingCancleButton.SetActive(true);
    }

    public void ShareButtonClick()
    {
        ShareTextInAnroid();
    }

    public void ShareTextInAnroid()
    {

        var shareSubject = "Fruit Slicer King!";
        var shareMessage = "Be Fruit Slicer King! " +
                           "Get the Fruit Slicer King For Free! \nDownload Now\n\n" +
                           "https://play.google.com/store/apps/details?id=com.leopardgames.fruitslicerking";

        //isProcessing = true;

        if (!Application.isEditor)
        {
            //Create intent for action send
            AndroidJavaClass intentClass =
                new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject =
                new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>
                ("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

            //put text and subject extra
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            intentObject.Call<AndroidJavaObject>
                ("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), shareSubject);
            intentObject.Call<AndroidJavaObject>
                ("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareMessage);

            //call createChooser method of activity class
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity =
                unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject chooser =
                intentClass.CallStatic<AndroidJavaObject>
                ("createChooser", intentObject, "Share your high score");
            currentActivity.Call("startActivity", chooser);
        }


        //yield return new WaitUntil();
        //isProcessing = false;
    }

    public void CheatCoinButton()
    {
        DataHandler.inst.Coins += 100;
        CoinScript.inst.cointest();
    }

    public void ExitStartYesButton()
    {
        Debug.LogError("Exitgame");
        Application.Quit();
    }

    public void ExitStartNoButton()
    {
        exitStartScreen.SetActive(false);
    }

}//class end
