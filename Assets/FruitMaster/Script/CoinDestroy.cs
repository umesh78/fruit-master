﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinDestroy : MonoBehaviour 
{
	public Transform coinObj;
	public float speed;

	void Start ()
	{
		speed = 5f;
		coinObj = FindObjectOfType<CoinScript>().transform;		
	}
	
	void Update ()
	{
		this.transform.position = Vector3.MoveTowards(this.transform.position, coinObj.transform.position, speed);
	}
    public void CoinDes()
    {

        Destroy(gameObject);
    }

}//class end
