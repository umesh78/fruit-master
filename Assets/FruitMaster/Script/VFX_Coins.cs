using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX_Coins : MonoBehaviour
{
    public static VFX_Coins Instance;
    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
    }

    // Start is called before the first frame updat
    [SerializeField]
    List<GameObject> coinList;
    public Transform targetPoint; 
    public Transform startPoint; 
   
 
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddCoins(5);
        }
    }

    public void AddCoins(int coins)
    {
        //if (DataHandler.Instance.GameSound==1)
        //{  
        //    auCoinMove.Play();
        //}
        if (startPoint!=null)
        {
            transform.position = startPoint.position;
        }
         
        for (int i = 0; i < coinList.Count; i++)
        {
            coinList[i].SetActive(false);
            coinList[i].transform.localPosition = Vector3.zero;
        }
         
        //DataHandler.Instance.Coins += coins;
        StartCoroutine(StartCoinPs());
    }

    IEnumerator StartCoinPs()
    {
        int r = Random.Range(1, coinList.Count);
        for (int i = 0; i < r; i++)
        { 
            coinList[i].SetActive(true);
            Vector3 fpos = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
            fpos = fpos.normalized * 1f ;
            
            coinList[i].GetComponent<VFXCoinsMove>().StartMoving(fpos, targetPoint.position);
            yield return new WaitForSeconds(0.1f);
        }
    }
}
