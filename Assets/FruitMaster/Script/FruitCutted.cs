﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitCutted : MonoBehaviour {

	void Start () {
        Invoke("MoveToMixer", .2f);
	}
	
	void MoveToMixer() {
        gameObject.layer = 8;
        GetComponent<Rigidbody2D>().drag = 0;
        GetComponent<Collider2D>().isTrigger = false;
        GetComponent<Collider2D>().isTrigger = false;
    }

}//class end
