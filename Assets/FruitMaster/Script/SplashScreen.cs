using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SplashScreen : MonoBehaviour
{  
    public Image fillImage;
    private void Start()
    {
        InvokeRepeating("SplashFillImage", 0, Random.Range(0.1f,1f));
    }
    float targetFillAmount = 0;
    private void Update()
    {
        if (fillImage.fillAmount < 1)
        {
            fillImage.fillAmount = Mathf.MoveTowards(fillImage.fillAmount, targetFillAmount, 0.4f * Time.deltaTime);
        }
    }

    public void SplashFillImage()
    {
        if (fillImage.fillAmount <= 1f)
        {
            targetFillAmount += Random.Range(0.05f, 0.2f);
            if (fillImage.fillAmount>=1)
            {
                if ( fillImage.fillAmount == 1)
                {
                    Debug.LogError(111);
                    gameObject.SetActive(false);
                    AdManager.inst.ShowBannerAd();
                }
            }
        }
    }


}//class end
